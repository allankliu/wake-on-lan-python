#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Wake on Lan

import socket
import struct


def WOL(macaddress):
        if len(macaddress) == 12:
                pass
        elif len(macaddress) == 12 + 5:
                sep = macaddress[2]
                macaddress = macaddress.replace(sep, '')
        else:
                raise ValueError('Incorrect MAC address format')

        data = ''.join(['FFFFFFFFFFFF', macaddress * 16])
        send_data = b''

        for i in range(0, len(data), 2):
                byte_dat = struct.pack('B', int(data[i: i + 2], 16))
                send_data = send_data + byte_dat

        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        sock.bind(('192.168.3.56', 20000))
        # TODO: change to the broadcast IP of your LAN
        sock.sendto(send_data, ('255.255.255.255', 9))
        sock.close()


if __name__ == '__main__':
        # TODO: change to the MAC of the Target NIC
        mynas = '24:5E:BE:68:CE:0D'
        #WOL('24:5E:BE:68:CE:0D') # mynas.local
        WOL(mynas)
        print("The device {} has been waken up.".format(mynas))

