# Wake On LAN Python

WOL is very useful in Ethernet based NAS and other devices. It takes me quite a long time
to find out why my code or 3rd party tools don't work in my PC. Because most of the code
or tools are running on WSL2, which is communicate in another subnet.

Therefore I test the code in Windows command line, and it works well with my QNAP NAS. 
I guess it should work in a Linux box with my QNAP as well.

## Plan

1. Test it in a Linux box such as RPi or other SBC.
2. Port it to ESP32 WiFi SoC, either in native IDF GCC or MicroPython.
3. Connect the ESP32 to a IoT PaaS via MQTT broker, so we can check status and turn it on
	remotely

Normally when the devices such as NAS/PC are turned on, it can communicate with some IoT
platforms and users can remotely turn it off as well.

## Web Blog

- https://www.servertribe.com/attune-hub/wake-on-lan-another-pc-via-wsl-on-windows-10-2019/

